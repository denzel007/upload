<?php

namespace Tetrapak07\Upload;

use Imagick;
use Supermodule\ControllerBase as SupermoduleBase;
use Tetrapak07\Upload\Uploader;
/**
 * UploadHelper - Helper for files delete, upload
 *
 */
class UploadHelper extends BaseHelper
{
    
    /**
     * @var Phalcon\Config
     */
    private $config;
    /**
     * @var MailHelper
     */
    static private $instance;

    /**
     * Initialize helper
     */
    private function __construct()
    {
        if (!SupermoduleBase::checkAndConnectModule('upload')) {
           header("Location: /error/show404");
           exit;
        }
        
        $this->setDi();
        $this->config = $this->di->get('config')->modules->upload;
    }

    /**
     * Get current instanse of this class
     * @return UploadHelper
     */
    static public function getInsctance()
    {
        if (empty(self::$instance)) {
            self::$instance = new self;
        }
        return self::$instance;
    }
   
    /**
     * Upload Image
     * 
     * @param string $action Type of action update/delete image
     * @param Request $request 
     * @param string|boolean $mainPath global path
     * @param boolean $userProfileImage if from user profile request
     * @param boolean|int $newWidth set new width
     * @return boolean|array
     */
    static public function uploadImage($request, $addPath = '', $mainPath = false, $userProfileImage = false, $newWidth = false, $onlyKey = false, $ignoreKey = false, $resize = false)
    {
        $ret = false; 
        $instance = self::getInsctance();
        if (!$mainPath) {
         $mainPath = $instance->config->gloabalDir;
        }
           
        $ret = $instance->uploadImg($request, $addPath, $mainPath, $userProfileImage, $newWidth, $onlyKey, $ignoreKey, $resize);
        return $ret; 
    } 
    
     /**
     * Upload Image
     * 
     * @param string $action Type of action update/delete image
     * @param Request $request 
     * @param string|boolean $mainPath global path
     * @return boolean|array
     */
    static public function uploadFiles($request, $addPath = '', $mainPath = false, $limit = 10, $maxSize = 10)
    {
        $ret = false; 
        $instance = self::getInsctance();
        if (!$mainPath) {
         $mainPath = $instance->config->gloabalDir;
        }
           
        $ret = $instance->upldFiles($request, $addPath, $mainPath, $limit, $maxSize);
        return $ret; 
    } 
    
     /**
     * Upload Image
     * 
     * @param string $action Type of action update/delete image
     * @param Request $request 
     * @param string|boolean $mainPath global path
     * @return boolean|array
     */
    static public function uploadFilesNew($fls, $addPath = '', $mainPath = false, $limit = 10, $maxSize = 10)
    {
        $ret = false; 
        $instance = self::getInsctance();
        if (!$mainPath) {
         $mainPath = $instance->config->gloabalDir;
        }
           
        $ret = $instance->upldFilesNew($fls, $addPath, $mainPath, $limit, $maxSize);
        return $ret; 
    } 
    
   /**
     * Delete Image
     * 
     * @param string $urlDel part url of deleting img
     * @param string|boolean $mainPath global path
     * @return boolean|array
     */
    static public function deleteImage($urlDel, $mainPath)
    {
        $ret = false; 
        $instance = self::getInsctance();
        if (!$mainPath) {
         $mainPath = $instance->config->gloabalDir;
        }
           
        $ret = $instance->deleteImg($urlDel, $mainPath);
        return $ret; 
    } 
    
     /**
     * Upload or Delete Video
      * 
     * @param string $action Type of action update/delete image
     * @param Request $request 
     * @param string $urlDel part url of deleting img 
     * @param string $mainPath main upload path with subdirs (subdirs - "image", "video")
     * @return boolean
     */
    static public function uploadVideo ($action, $request, $mainPath = false)
    {
        $ret = false; 
        $instance = self::getInsctance();
        if (!$mainPath) {
         $mainPath = $instance->config->gloabalDir;
        }
           
       $ret = $instance->uploadVid($action, $request, $mainPath);
       return $ret;
    } 
    
    /**
     * Delete Video
     * 
     * @param string $urlDel part url of deleting img
     * @param string|boolean $mainPath global path
     * @return boolean|array
     */
    static public function deleteVideo($urlDel, $mainPath)
    {
        $ret = false; 
        $instance = self::getInsctance();
        if (!$mainPath) {
         $mainPath = $instance->config->gloabalDir;
        }
           
        $ret = $instance->deleteVid($urlDel, $mainPath);
        return $ret; 
    } 
    
     /* 
     * Image upload main function
     *  
     * @param Request $request 
     * @param string|boolean $mainPath global path
     * @param boolean $userProfileImage if from user profile request
     * @param boolean|int $newWidth set new width
     * @return boolean|array
     */
    private function upldFiles($request, $addPath, $mainPath, $limit = 10, $maxSize = 10)
    {
       $ret = false;

       $uploadDir = $this->config->uploadDir.'/'.$this->config->filesSubDir.'/'.$addPath;

       if ($request->hasFiles() == true) {
           
            $dir = $mainPath.DS.$uploadDir.DS; #full path
                       
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
           
            $uploader = new Uploader();
            $data = $uploader->upload($_FILES['files'], array(
                'limit' => $limit, //Maximum Limit of files. {null, Number}
                'maxSize' => $maxSize, //Maximum Size of files {null, Number(in MB's)}
                'extensions' =>$this->config->allowExtsAll->toArray(),//array('jpg', 'png', 'gif', 'bmp', 'jpeg', 'pdf', 'doc', 'docx', 'xls', 'xlsx'), //null, //Whitelist for file extension. {null, Array(ex: array('jpg', 'png'))}
                'required' => false, //Minimum one file is required for upload {Boolean}
                'uploadDir' => $dir, //Upload directory {String}
                'title' => array('auto', 12),//array('name'), //New file name {null, String, Array} *please read documentation in README.md
                'removeFiles' => true, //Enable file exclusion {Boolean(extra for jQuery.filer), String($_POST field name containing json data with file names)}
                'replace' => false, //Replace the file if it already exists  {Boolean}
                'perms' => null, //Uploaded file permisions {null, Number}
                'onCheck' => null, //A callback function name to be called by checking a file for errors (must return an array) | ($file) | Callback
                'onError' => null, //A callback function name to be called if an error occured (must return an array) | ($errors, $file) | Callback
                'onSuccess' => null, //A callback function name to be called if all files were successfully uploaded | ($files, $metas) | Callback
                'onUpload' => null, //A callback function name to be called if all files were successfully uploaded (must return an array) | ($file) | Callback
                'onComplete' => null, //A callback function name to be called when upload is complete | ($file) | Callback
                'onRemove' => null //A callback function name to be called by removing files (must return an array) | ($removed_files) | Callback
            ));
            $ret = [];
            if($data['isComplete']){
                $files = $data['data'];
                foreach ($files['metas'] as $key => $value) {
                    $type = NULL;
                    if ($value['type']) {
                    $type = $value['type'][0].'/'.$value['type'][1];
                    }
                    $namePart = explode('.', $value['name']);
                    
                    $ret[$key]['aspectRatio'] = NULL;
                            $ret[$key]['imageUrl'] = '/'.$uploadDir.'/'.$value['name'];
                            $ret[$key]['name'] = $value['name'];
                            $ret[$key]['type'] = $type;
                            $ret[$key]['size'] = $value['size'];
                            $ret[$key]['oldName'] = $value['old_name'];
                            $ret[$key]['file'] =  $value['name'];
                            $ret[$key]['url'] =  '/'.$uploadDir.'/'.$value['name'];
                            $ret[$key]['width'] =  NULL;
                            $ret[$key]['height'] =  NULL;
                }
                return $ret;
            }

            if($data['hasErrors']){
                $errors = $data['errors'];
                $ret['errors'] = $errors;
                return $ret;
            }
       }
    } 
    
    /* 
     * Image upload main function
     *  
     * @param Request $request 
     * @param string|boolean $mainPath global path
     * @param boolean $userProfileImage if from user profile request
     * @param boolean|int $newWidth set new width
     * @return boolean|array
     */
    private function upldFilesNew($fls, $addPath, $mainPath, $limit = 10, $maxSize = 10)
    {
       $ret = false;

       $uploadDir = $this->config->uploadDir.'/'.$this->config->filesSubDir.'/'.$addPath;

       if (count($fls)>0) {
           
            $dir = $mainPath.DS.$uploadDir.DS; #full path
                       
            if (!file_exists($dir)) {
                mkdir($dir, 0777, true);
            }
           
            $uploader = new Uploader();
            $data = $uploader->upload($fls, array(
                'limit' => $limit, //Maximum Limit of files. {null, Number}
                'maxSize' => $maxSize, //Maximum Size of files {null, Number(in MB's)}
                'extensions' =>$this->config->allowExtsAll->toArray(),//array('jpg', 'png', 'gif', 'bmp', 'jpeg', 'pdf', 'doc', 'docx', 'xls', 'xlsx'), //null, //Whitelist for file extension. {null, Array(ex: array('jpg', 'png'))}
                'required' => false, //Minimum one file is required for upload {Boolean}
                'uploadDir' => $dir, //Upload directory {String}
                'title' => array('auto', 12),//array('name'), //New file name {null, String, Array} *please read documentation in README.md
                'removeFiles' => true, //Enable file exclusion {Boolean(extra for jQuery.filer), String($_POST field name containing json data with file names)}
                'replace' => false, //Replace the file if it already exists  {Boolean}
                'perms' => null, //Uploaded file permisions {null, Number}
                'onCheck' => null, //A callback function name to be called by checking a file for errors (must return an array) | ($file) | Callback
                'onError' => null, //A callback function name to be called if an error occured (must return an array) | ($errors, $file) | Callback
                'onSuccess' => null, //A callback function name to be called if all files were successfully uploaded | ($files, $metas) | Callback
                'onUpload' => null, //A callback function name to be called if all files were successfully uploaded (must return an array) | ($file) | Callback
                'onComplete' => null, //A callback function name to be called when upload is complete | ($file) | Callback
                'onRemove' => null //A callback function name to be called by removing files (must return an array) | ($removed_files) | Callback
            ));
            $ret = [];
            if($data['isComplete']){
                $files = $data['data'];
                foreach ($files['metas'] as $key => $value) {
                    $type = NULL;
                    if ($value['type']) {
                    $type = $value['type'][0].'/'.$value['type'][1];
                    }
                    $namePart = explode('.', $value['name']);
                    
                    $ret[$key]['aspectRatio'] = NULL;
                            $ret[$key]['imageUrl'] = '/'.$uploadDir.'/'.$value['name'];
                            $ret[$key]['name'] = $value['name'];
                            $ret[$key]['type'] = $type;
                            $ret[$key]['size'] = $value['size'];
                            $ret[$key]['oldName'] = $value['old_name'];
                            $ret[$key]['file'] =  $value['name'];
                            $ret[$key]['url'] =  '/'.$uploadDir.'/'.$value['name'];
                            $ret[$key]['width'] =  NULL;
                            $ret[$key]['height'] =  NULL;
                }
                return $ret;
            }

            if($data['hasErrors']){
                $errors = $data['errors'];
                $ret['errors'] = $errors;
                return $ret;
            }
       }
    }  
    
    
     /* 
     * Image upload main function
     *  
     * @param Request $request 
     * @param string|boolean $mainPath global path
     * @param boolean $userProfileImage if from user profile request
     * @param boolean|int $newWidth set new width
     * @return boolean|array
     */
    private function uploadImg($request, $addPath, $mainPath, 
            $userProfileImage = false, $newWidth = false, $onlyKey = false, $ignoreKey = false, $resize = false)
    {
       $ret = false;
       $sizesArr = $this->config->resizeWidths->toArray();

       $uploadDir = $this->config->uploadDir.'/'.$this->config->imageSubDir.'/'.$addPath;

       if ($request->hasFiles() == true) {

            foreach ($request->getUploadedFiles() as $key => $file) {
                
                    if (($onlyKey)AND(is_string($onlyKey))) {
                        if ($file->getKey() !== $onlyKey) {
                            continue;
                        }
                    }
                    if (($ignoreKey)AND(is_string($ignoreKey))) {
                        if ($file->getKey() === $ignoreKey) {
                           continue;
                        }
                    }
                    
                    $mimeTypes = ['image/jpeg', 'image/pjpeg', 'image/gif', 'bmp', 'image/png', 'image/bmp', 'image/x-windows-bmp'];
                    if (empty($file->getTempName())) {
                        continue;
                    }
                    $mime = $file->getRealType();

                    if (in_array($mime, $mimeTypes)) {
                        
                        $dir = $mainPath.DS.$uploadDir.DS; #full path
                       
                        if (!file_exists($dir)) {
                            mkdir($dir, 0777, true);
                        }
                         $oldName = $file->getName();
                         $keyF = $file->getKey();
                         $hash = md5($keyF . microtime());
                         $filePath = $hash . '.jpg';

                        if (!$newWidth) {
                          # $imageFieId = end((explode('.', $keyF)));//end((explode('.', $file_name)));
                          # $width = $imageFieId == 'logo' ? $this->config->logoWidth : $this->config->imgWidth;
                            $width = $this->config->imgWidth;
                         } else {
                           $width =  $newWidth; 
                         }

                        #$filePath = md5($imgId. microtime()) . '.jpg';
                        list($originalWidth, $originalHeight) = getimagesize($file->getTempName());
                        $aspectRatio = $originalHeight / $originalWidth;

                        $image = new Imagick($file->getTempName());
                        $size = $image->getImageLength();

                        if ($originalWidth > $width) {
                            $height = $originalHeight - ( $originalWidth - $width ) * $aspectRatio;
                            $image->adaptiveResizeImage($width, $height);
                        }
                        
                        $image->setImageFormat('jpg');
                                               
                        if ($image->writeImage($dir.$filePath)) {
                            if ($userProfileImage) {
                                unlink($mainPath.$userProfileImage);
                            }
                            $ret[$key]['aspectRatio'] = $aspectRatio;
                            $ret[$key]['imageUrl'] = '/'.$uploadDir.'/'.$filePath;
                            $ret[$key]['name'] = $hash;
                            $ret[$key]['type'] = 'image/jpeg';
                            $ret[$key]['size'] = $size;
                            $ret[$key]['oldName'] = $oldName;
                            $ret[$key]['file'] =  $filePath;
                            $ret[$key]['url'] =  '/'.$uploadDir.'/'.$filePath;
                            $ret[$key]['width'] =  $width;
                            $ret[$key]['height'] =  $height;
                            
                            if ((count($sizesArr)> 0)&&($resize)) {
                                foreach ($sizesArr as $key => $widthN) {
                                    
                                    $img[$key]  = new Imagick($file->getTempName());
                                  
                                   if ($originalWidth > $widthN) {
                                        
                                        $height = $originalHeight - ( $originalWidth - $widthN ) * $aspectRatio;
                                        $img[$key]->adaptiveResizeImage($widthN, $height);
                                        $img[$key]->setImageFormat('jpg'); 
                                        $filePath = $hash . '-'.$widthN.'px.jpg';
                                        $img[$key]->writeImage($dir.$filePath);
                                    }
                                }
                            }
                            
                        }

                    }
                    
            }
       }
       
      return $ret;  
    }
    
    private function deleteImg($imageUrl , $mainPath)
    {
        $ret = false;
        unlink($mainPath.$imageUrl);
        $ret[0]['aspectRatio'] = 0;
        $ret[0]['imageUrl'] = '';
        return $ret;
    }
    
    private function deleteVid($imageUrl , $mainPath) {
        # TO DO
    }
    
    private function uploadVid($action, $request, $mainPath) {
        # TO DO
    }
  
} 