<?php

namespace App\Helpers;

use  App\Controllers\ControllerBase;

/**
 * UploadHelper - Helper for files delete, upload
 *
 */
class UploadHelper extends \Tetrapak07\Upload\UploadHelper
{
    
    private function __construct()
    {
        parent::__construct();
        
        $base = new ControllerBase();
        
        $base->onConstruct();
    }
    
}    
